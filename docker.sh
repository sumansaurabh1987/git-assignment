#!suman saurabh 
assignment git /bin/sh

# on develop branch, docker.sh will tag extractor with tag of develop and push to docker hub repos (you can specify the your own server)
# on master branch, you can specify a list of VERSION and release.sh will tag them and push to docker hub repos.
# on other branches, docker.sh will not do docker tag and push.

# variables that can be set
# DEBUG   : set to echo to print command and not execute

# exit on error, with error code
set -e

# can use the following to push to isda-registry for testing:
# BRANCH="master" SERVER=isda-registry.ncsa.illinois.edu/

# use DEBUG=echo to print all commands
# DEBUG="echo"
export DEBUG=${DEBUG:-""}

# use SERVER=XYZ/ to push to a different server
SERVER=${SERVER:-""}

# docker image name
NAME=${NAME:-"bdcli"}
PROJECT=${PROJECT:-"browndog"}

# what branch are we on
BRANCH=${BRANCH:-"$(git rev-parse --abbrev-ref HEAD)"}

# make sure docker is build
${DEBUG} docker build -t ${PROJECT}/${NAME}:latest .

# find out the version
if [ "${BRANCH}" = "master" ]; then
    VERSION=${VERSION:-"0.4.2 0.4 0 latest"}
elif [ "${BRANCH}" = "develop" ]; then
    VERSION="develop"
else
    exit 0
fi

# tag all images and push if needed
for v in ${VERSION}; do
    if [ "$v" != "latest" -o "$SERVER" != "" ]; then
        ${DEBUG} docker tag ${PROJECT}/${NAME}:latest ${SERVER}${PROJECT}/${NAME}:${v}
    fi
    ${DEBUG} docker push ${SERVER}${PROJECT}/${NAME}:${v}
    if [ "$v" != "latest" -o "$SERVER" != "" ]; then
        ${DEBUG} docker rmi ${SERVER}${PROJECT}/${NAME}:${v}
    fi
done
${DEBUG} docker rmi ${PROJECT}/${NAME}:latest
