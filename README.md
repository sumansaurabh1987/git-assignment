Brown Dog Command-Line Interface (BD-CLI)
=========================================
this is new change by suman
bd [OPTION] [FILE/DIRECTORY]
-----------------------------

Command line interface to Brown Dog services. Use to convert files between formats and extract various derived products 
from file contents (e.g. tags, metadata, signatures, previews).

-b set the BD URL (e.g. https://bd-api.ncsa.illinois.edu)

-t set the token

-v verbose output

-w set wait time for conversions/extractions

-h display help

-o set output format 

--outputs list the available output formats for the given file

--extractors list the available extractors for the given file

--find search current directory for a file similar to the given file

--bigdata use this flag if the file is large enough and it needs to be processed locally rather than using remote
 Brown Dog services

**Setup**

Before using BD-CLI you will need to sign up with Brown Dog and create an account at 
[https://browndog.ncsa.illinois.edu]. Most of the commands offered by BD-CLI will require you to have a temporary token 
generated by Brown Dog. The first time you are running a command that requires this token, BD-CLI will prompt you for 
your Brown Dog username and password and get a key and token from your Brown Dog instance, the default being 
[bd-api.ncsa.illinois.edu]. The generated key and token will be written to a file (.bdcli) in your home directory. 
The next time onwards BD-CLI will reuse this key and generate new tokens when the current token expires. 
You can alternatively use '-t' option to pass in a token that has already been generated (e.g. through BDFiddle or 
another application).

**Examples:**

List available output formats for given file:

		bd --outputs image.pcd

Convert the given Kodak Photo CD image to a JPEG image.  Produces file image.jpg:

		bd -o jpg image.pcd

Convert a directory of images into the JPEG format:

		bd -o jpg images/

List available extractors for given file:

		bd --extractors image.jpg

Extract data from files contents (e.g. tags, previews, analysis, and other derived products).  Produces file image.json:

		bd image.jpg

Chain conversion and extraction.  Equivalent to the previous two examples:

		bd -o jpg image.pcd | bd

Index files within a directory:

		bd images/

Search for a similar file within a directory that has already been indexed:

		bd --find /path/image.jpg

Read input filename from a text file. This feature can be used to integrate BD-CLI with other programs by passing
filename around. Here input.txt can contain one input filename:

		bd < input.txt
		bd -o jpg < input.txt


Installation
-----------------------

You can install this Python client using:

```pip install bdcli```

or 


```python setup.py install```

