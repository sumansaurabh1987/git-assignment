# Change Log for BD-CLI
All notable changes to this repo will be documented here.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [0.5.0] - 2018-12-19
### Added
- Feature to submit file to specific extractor [BD-2212](https://opensource.ncsa.illinois.edu/jira/browse/BD-2212).
- Feature to send extractor parameters [BD-2213](https://opensource.ncsa.illinois.edu/jira/browse/BD-2213)

## [0.4.2] - 2018-10-12
### Changed
- Dockerfile and docker.sh does version tagging and installing from source instead of PyPi. [BD-2278](https://opensource.ncsa.illinois.edu/jira/browse/BD-2278)

## [0.4.1] - 2018-10-04
### Fixed
- Bug in extraction code related to changed method signature in BD.py [BD-2279](https://opensource.ncsa.illinois.edu/jira/browse/BD-2279)

## [0.4.0]- 2018-09-26
### Added
- Change log